# NextPipes

NextPipes is a series of Nextflow pipelines for various types of bioinformatics analyses. The concept behind NextPipes was to create a simple, reproducible, and easily configurable series of pipelines with minimal dependencies. Ideally, NextPipes is run in a containerized environment such as Docker or Singularity.

## Installation

NextFlow depends on java, while NextPipes also uses docker for containerization.

### JDK and NextFlow

First, install  java sdk [>= 11, <= 18](https://www.nextflow.io/docs/latest/getstarted.html#requirements). On [Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-20-04):

```bash
sudo apt install default-jdk
```

Then install NextFlow as per https://www.nextflow.io/docs/latest/getstarted.html#installation

First, download and run executable, with either

```bash
wget -qO- https://get.nextflow.io | bash
```

or

```bash
curl -s https://get.nextflow.io | bash
```

Then add permissions for read/execution:

```bash
chmod +xr nextflow
```

Optionally, move the nextflow file to a directory accessible by your `$PATH`. Otherwise you will need to call nextflow via its absolute or relative path. E.g.

```bash
mv nextflow $HOME/.local/bin
```

### Install Docker

Install docker desktop: https://docs.docker.com/desktop/
I also needed to install `cgroup-tools` to prevent docker from erroring

### Install NextPipes

Clone the NextPipes gitlab repository: https://gitlab.partners.org/ksg/hector/nextpipes

- [instructions for GitHub Desktop](https://itnext.io/how-to-use-github-desktop-with-gitlab-cd4d2de3d104)
- or run `git clone nextpipes.git`

For convienvce, add the following to `~/.bashrc` replacing `/path/to` with the actual absolute path of your NextPipes repo.

```bash
export NEXTPIPES=/path/to/nextpipes/
```

Assuming everything went smoothly during installation, you should be able to run any NextPipes pipeline using the `nextflow` command.

It is suggested that the user familiarize themselves with the [Nextflow DSL](https://www.nextflow.io/docs/latest/basic.html). This will provide you with a background on the more advanced configuration options, and how NextPipes actually runs under the hood should you choose to explore the source code further.

### Run a test

```bash
nextflow run ./bin/test.nf -profile local
N E X T F L O W  ~  version 22.10.1
Launching `bin/test.nf` [pensive_swanson] DSL1 - revision: d74ba07f20
executor >  local (1)
[c5/34bc68] process > test [100%] 1 of 1 ✔
Congrats! You just ran a test.
```

If everything went smoothly, you should see "Congrats! You just ran a test". This workflow runs a simple echo statement within a Ubuntu image.

## realign_bam

This test pipeline realigns a passed bam file. This is currently being used for benchmarking of different configurations of storage and compute environments.
It relies on a reference human genome, which is stored for convience on ERISone.

### Mount ERISone `ksg-profile-share` reference data

First, create an `.smbcredentials` file in the form (likely in `$HOME`):

```txt
user: <partners id>
password: <partners password>
```

To run on startup on the OS, add the following to `/etc/fstab` and then run `sudo mount -a` to execute the mount:

```txt
//erisonefs.partners.org/ksg-profile-share/md1164/hector/ref_data	/path/to/nextpipes/ref_data	cifs	credentials=/path/to/.smbcredentials,iocharset=utf8,domain=PARTNERS,vers=2.0	0	0
```

For a one-time mount, run:

```bash
sudo mount -t cifs -o vers=2.0,user=<partners id>,password=<partners password>,domain=PARTNERS //erisonefs.partners.org/ksg-profile-share $NEXTPIPES/ref_data
```

### Run the nextflow test process locally

```bash
nextflow run bin/realign_bam.nf -profile local,docker,grch37,dynamic_memory --input_bam <input bam>
```

In the above example, Nextflow uses various profiles to pre-define certain configuration settings with the `-profile` argument (note the single dash).

These are defined in the `nextflow.config` file, which then imports various "configs".
Look further into each of the files (in `/conf`) and you'll find a number of profiles defined for features like docker integration, reference management, executors, computing resources, etc.
