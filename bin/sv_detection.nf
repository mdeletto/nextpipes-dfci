#!/usr/bin/env nextflow

def helpMessage() {
    log.info"""
    ===================================
     sv_detection.nf --options
    ===================================
    Description:

    This workflow is a re-implementation of the structural variant detection methodology used in vega/pipelines.

    Usage:

    nextflow run sv_detection.nf -profile singularity,grch37_local,dynamic_memory \
        --tumor_bam '/path/to/*.{bam,bai}' \
        --normal_bam '$NEXTPIPES/test_data/ceph/PBP-9d13bb9b-27e3-4aff-af15-aa461102b0d2.bam'
        --mode 'somatic' \
        -c /path/to/nextflow.config

    Mandatory arguments:
      -profile                      Suggested profiles are "grch37,local,local_docker,std_resources" or "aws,aws_docker,aws_grch37,std_resources"
      --tumor_bam 		            Glob pattern to tumor BAM file. Please note you should supply your pattern as '/path/to/mysample.{bam,bai} to capture both BAM file and its index file.'
      --normal_bam 		            Path to normal BAM file (e.g. /path/to/normal.bam). At this time, this workflow only supports a single control BAM.'
      --mode                        Should be either 'somatic' or 'germline'
      --ref_name                    Reference name with path prefix (e.g. /path/to/grch37). Reference fasta, fai, and other indexes will be autodetected based on this prefix.
      --manta_config                Config file for Manta SV caller
      --ref_target_bed              Assay-specific target BED
      --ref_repeat_mask_bed         Assay-specific repeat mask BED
      --ref_pon_range_tsv           Assay-specific panel of normal (PON) range tsv
      --ref_filter_list_tsv         Assay-specific filter list tsv
      --ref_blacklist_bed_tsv       Assay-specific blacklist BED file

    When using profile `aws` you must also provide an S3 bucket as working directory via one of:
      -bucket-dir
      -work-dir

    Other options:
      --output_dir           The output directory where the results will be saved. Default is current working directory.

    """.stripIndent()
}

// Show help message
params.help = false
if (params.help){
    helpMessage()
    exit 0
}

// ARGUMENTS
output_dir = params.output_dir ? params.output_dir : "."

// REFERENCES
manta_config = file("${params.manta_config}")
ref_genome_files = file("${params.ref_name}*")
ref_target_bed = file("${params.ref_target_bed}")
ref_repeat_mask_bed = file("${params.ref_repeat_mask_bed}")
ref_pon_range_tsv = file("${params.ref_pon_range_tsv}")
ref_filter_list_tsv = file("${params.ref_filter_list_tsv}")
ref_blacklist_bed_tsv = file("${params.ref_blacklist_bed_tsv}")
normal_bam = file("${params.normal_bam}" ?: '$NEXTPIPES/test_data/ceph/PBP-9d13bb9b-27e3-4aff-af15-aa461102b0d2.bam')
normal_bam_index = file("${params.normal_bam}.bai")

process detectSVs_svaba {

  label 'svaba'
  publishDir "$output_dir/${tumor_bam.simpleName}/${task.process}/", mode : 'symlink', overwrite : true

  input:
  tuple path(tumor_bam), path(tumor_bam_index)
  path normal_bam
  path normal_bam_index
  path ref_genome_files

  output:
  // See: https://github.com/walaj/svaba#output-String-description
  tuple val("${tumor_bam.simpleName}"), path("${tumor_bam.simpleName}.contigs.bam"), path("${tumor_bam.simpleName}.svaba.unfiltered.{germline,somatic}.{sv,indel}.vcf")

  script:
  def ref_fasta = ref_genome_files.find{ it =~ '.(fa|fasta)$'}

  """
  svaba run \
    -t $tumor_bam \
    -n $normal_bam \
    -G $ref_fasta \
    -a ${tumor_bam.simpleName} \
    -p ${task.cpus}
  """
}

process detectSVs_manta {

  label 'manta'
  publishDir "$output_dir/${tumor_bam.simpleName}/${task.process}/", mode : 'symlink', overwrite : true

  input:
  tuple path(tumor_bam), path(tumor_bam_index)
  path normal_bam
  path normal_bam_index
  path manta_config
  path ref_genome_files

  output:
  tuple val("${tumor_bam.simpleName}"), path("somaticSV.vcf")

  script:
  def ref_fasta = ref_genome_files.find{ it =~ '.(fa|fasta)$'}

  """
  set -e

  configManta.py \
    --config $manta_config \
    --normalBam $normal_bam \
    --tumorBam $tumor_bam \
    --referenceFasta $ref_fasta \
    --outputContig \
    --exome \
    --runDir ./output/

  rm -rf ./output/workspace/pyflow.data/logs/tmp

  ./output/runWorkflow.py -m local

  SAMTOOLS_EXE_PATH=\$(which samtools)

  convertInversion.py \$SAMTOOLS_EXE_PATH \
    $ref_fasta \
    ./output/results/variants/somaticSV.vcf.gz > somaticSV.vcf ;
  """
}

process indexBAMs {

  label 'samtools'
  stageInMode 'copy'
  //publishDir "$output_dir/${tumor_bam.simpleName}/${task.process}/", mode : 'symlink', overwrite : true

  input:
  tuple val(prefix), path('*')

  output:
  tuple path("${prefix}.output.bam"), path("${prefix}.output.bam.bai")

  script:
  """
  #!/bin/bash

  bam_count=\$(ls *.bam | wc -l)
  bai_count=\$(ls --ignore=*.bam | wc -l)

  #

  if [[ \$bam_count -eq 1 ]] && [[ \$bai_count -eq 0 ]]
  then
    cat ${prefix}.bam > ${prefix}.output.bam
    samtools index -@{task.cpus} ${prefix}.output.bam ${prefix}.output.bam.bai
  elif [[ \$bam_count -eq 1 ]] && [[ \$bai_count -eq 1 ]]
  then
    cat ${prefix}.bam > ${prefix}.output.bam
    cat ${prefix}*.bai > ${prefix}.output.bam.bai
  else
    exit 1
  fi
  """
}

process sv_post {

  label 'sv_post'
  publishDir "$output_dir/$prefix/${task.process}/", mode : 'symlink', overwrite : true

  input:
  tuple val(prefix), path(in_svaba_bam), path(in_svaba_vcfs), path(in_manta_vcf)
  val mode
  path ref_target_bed
  path ref_repeat_mask_bed
  path ref_pon_range_tsv
  path ref_filter_list_tsv
  path ref_blacklist_bed_tsv

  output:
  path "${prefix}.${mode}SVs.tsv"

  script:
  """
  set -e

  /app/ensemble_calling.R \
    --svaba-indel ${prefix}.svaba.unfiltered.${mode}.indel.vcf \
    --svaba-sv ${prefix}.svaba.unfiltered.${mode}.sv.vcf \
    --svaba-bam $in_svaba_bam \
    --manta-sv-indel $in_manta_vcf \
    --target-bed $ref_target_bed \
    --repeat-mask $ref_repeat_mask_bed \
    --pon-range $ref_pon_range_tsv \
    --breakmer-filter-list $ref_filter_list_tsv \
    --blacklist $ref_blacklist_bed_tsv \
    --mode $mode \
    --output ${prefix}.${mode}SVs.tsv
  """
}

workflow {

    // RUN WORKFLOW
    in_bams = Channel.fromFilePairs(params.tumor_bam, size: -1)
    indexBAMs(in_bams)
    detectSVs_svaba(indexBAMs.out, normal_bam, normal_bam_index, ref_genome_files)
    detectSVs_manta(indexBAMs.out, normal_bam, normal_bam_index, manta_config, ref_genome_files)
    sv_post(detectSVs_svaba.out.join(detectSVs_manta.out), params.mode, ref_target_bed, ref_repeat_mask_bed, ref_pon_range_tsv, ref_filter_list_tsv, ref_blacklist_bed_tsv)

}
