#!/usr/bin/env nextflow

def helpMessage() {
    log.info"""
    ===================================
     realign_bam.nf --options
    ===================================
    Description:

    This is a basic workflow for realigning input BAM files, primarily used for testing.

    Usage:

    nextflow run realign_bam.nf -profile docker,local,grch37,std_resources --input_bam '/path/to/*.bam'

    Mandatory arguments:
      -profile             Suggested profiles are "grch37,local,local_docker,std_resources" or "aws,aws_docker,aws_grch37,std_resources"
      --input_bam 		     Glob pattern to input BAM file. Multiple BAMs can be captured with a wildcard (e.g. see usage)
      --ref_name           Reference name with path prefix (e.g. /path/to/grch37). Reference fasta, fai, and other indexes will be autodetected based on this prefix.
      --dbsnp_vcf          Path to dbSNP vcf.

    When using profile `aws` you must also provide an S3 bucket as working directory via one of:
      -bucket-dir
      -work-dir

    Other options:
      --output_dir           The output directory where the results will be saved. Default is current working directory.

    """.stripIndent()
}

// Show help message
params.help = false
if (params.help){
    helpMessage()
    exit 0
}

// ARGUMENTS
output_dir = params.output_dir ? params.output_dir : "."

// REFERENCES
dbsnp_vcf = file("${params.dbsnp_vcf}")
dbsnp_vcf_index = file("${params.dbsnp_vcf}.idx")
references = file("${params.ref_name}*")

process revertBam {

  label 'gatk4'
  publishDir "$output_dir/${bam.simpleName}/${task.process}/", mode : 'symlink', overwrite : true

  input:
  file bam

  output:
  tuple val("${bam.simpleName}"), file("${bam.simpleName}_OQ.ba*")

  script:
  def java_memory = "${task.memory}".replaceAll('B', '').replaceAll(' ', '')

  """
  gatk --java-options "-Xmx${java_memory}" RevertSam \
    -I $bam \
    -O ${bam.simpleName}_OQ.bam \
    --RESTORE_ORIGINAL_QUALITIES \
    --CREATE_INDEX \
    --TMP_DIR=$TMPDIR
  """
}

process bamToFastq {

  label 'gatk4'
  publishDir "$output_dir/$prefix/${task.process}/", mode : 'symlink', overwrite : true

  input:
  tuple val(prefix), file("*")

  output:
  tuple val(prefix), file("${prefix}_R1.fastq"), file("${prefix}_R2.fastq"), file("${prefix}_unpaired.fastq")

  script:
  def java_memory = "${task.memory}".replaceAll('B', '').replaceAll(' ', '')

  """
  gatk --java-options "-Xmx${java_memory}" SamToFastq \
    -I ${prefix}_OQ.bam \
    -F ${prefix}_R1.fastq \
    -F2 ${prefix}_R2.fastq \
    -FU ${prefix}_unpaired.fastq \
    --VALIDATION_STRINGENCY LENIENT \
    --TMP_DIR=$TMPDIR
  """
}


process performAlignment {

  label 'bwa'
  publishDir "$output_dir/$prefix/${task.process}/", mode : 'symlink', overwrite : true

  input:
  tuple val(prefix), file(R1_fastq), file(R2_fastq), file(unpaired_fastq)
  file ref_name

  output:
  tuple val(prefix), file("${prefix}.sam")

  script:
  def ref_fasta = ref_name.find{ it =~ '.(fa|fasta)$'}

  """
   bwa mem -t ${task.cpus} \
        $ref_fasta \
        $R1_fastq \
        $R2_fastq \
        > ${prefix}.sam
  """
}

process addOrReplaceReadGroups {

  label 'gatk4'
  //scratch true
  publishDir "$output_dir/$prefix/${task.process}/", mode : 'symlink', overwrite : true

  input:
  tuple val(prefix), file(sam)

  output:
  tuple val(prefix), file("${prefix}.ba*")

  script:
  def java_memory = "${task.memory}".replaceAll('B', '').replaceAll(' ', '')

  """
  gatk --java-options "-Xmx${java_memory}" AddOrReplaceReadGroups \
    -I $sam \
    -O ${prefix}.bam \
    --SORT_ORDER queryname \
    --RGID=4 \
    --RGLB=lib1 \
    --RGPL=illumina \
    --RGPU=unit1 \
    --RGSM=${prefix} \
    --VALIDATION_STRINGENCY LENIENT \
    --TMP_DIR=$TMPDIR
  """
}

process markDuplicates {

  label 'gatk4'
  publishDir "$output_dir/$prefix/${task.process}/", mode : 'symlink', overwrite : true

  input:
  tuple val(prefix), file("*")

  output:
  tuple val(prefix), file("${prefix}_dedup.ba*")

  script:
  def java_memory = "${task.memory}".replaceAll('B', '').replaceAll(' ', '')

  """
  gatk --java-options "-Xmx${java_memory}" MarkDuplicates \
    -I ${prefix}.bam \
    -O ${prefix}_dedup_RN.bam \
    -M ${prefix}_dedup_RN.txt \
    --VALIDATION_STRINGENCY LENIENT \
    --TMP_DIR=$TMPDIR

  gatk --java-options "-Xmx${java_memory}" SortSam \
    -I ${prefix}_dedup_RN.bam \
    -O ${prefix}_dedup.bam \
    -SO coordinate \
    --VALIDATION_STRINGENCY LENIENT \
    --TMP_DIR=$TMPDIR

  gatk --java-options "-Xmx${java_memory}" BuildBamIndex \
    -I ${prefix}_dedup.bam \
    --VALIDATION_STRINGENCY LENIENT \
    --TMP_DIR=$TMPDIR
  """
}

process performBaseQualityScoreRecalibration {

  label 'gatk4'
  publishDir "$output_dir/$prefix/${task.process}/", mode : 'symlink', overwrite : true

  input:
  tuple val(prefix), file("*")
  file dbsnp_vcf
  file dbsnp_vcf_index
  file ref_name

  output:
  tuple val(prefix), file("${prefix}_dedup_bqsr.ba*")

  script:
  def ref_fasta = ref_name.find{ it =~ '.(fa|fasta)$'}
  def java_memory = "${task.memory}".replaceAll('B', '').replaceAll(' ', '')

  """
   gatk --java-options "-Xmx${java_memory}" BaseRecalibrator \
    -R $ref_fasta \
    -I ${prefix}_dedup.bam \
    -O ${prefix}_bqsr.table \
    --known-sites $dbsnp_vcf \
    -OQ 

   gatk --java-options "-Xmx${java_memory}" ApplyBQSR \
    -R $ref_fasta \
    -I ${prefix}_dedup.bam \
    -O ${prefix}_dedup_bqsr.bam \
    -bqsr ${prefix}_bqsr.table \
    --create-output-bam-md5 \
    --emit-original-quals 
  """
}


workflow {

    // RUN WORKFLOW
    in_bams = Channel.fromPath("${params.input_bam}")
    revertBam(in_bams)
    bamToFastq(revertBam.out)
    performAlignment(bamToFastq.out, references)
    addOrReplaceReadGroups(performAlignment.out)
    markDuplicates(addOrReplaceReadGroups.out)
    performBaseQualityScoreRecalibration(markDuplicates.out, dbsnp_vcf, dbsnp_vcf_index, references)

}
