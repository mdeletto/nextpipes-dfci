#!/usr/bin/env nextflow

import groovy.json.*

process test {

  label 'bash'

  output:
  stdout

  script:
  """
  echo 'Congrats! You just ran a test.'
  """

}

workflow {
  test | view
}
