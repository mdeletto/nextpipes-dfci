## Singularity

Apptainer/Singularity is the most widely used container system for HPC. It is designed to execute applications at bare-metal performance while being secure, portable, and 100% reproducible. 

## Installation

```bash
# For example, on Ubuntu/Debian based OSes
sudo apt update
wget https://github.com/sylabs/singularity/releases/download/v3.9.7/singularity-ce_3.9.7-bionic_amd64.deb
sudo apt install ./singularity-ce_3.9.7-bionic_amd64.deb
# Or download here and install with the appropriate package manager
# https://github.com/sylabs/singularity/releases/
```

## Configuration

Once Singularity is installed, you should be able to use it with Nextflow using the `-profile singularity` argument (see `$NEXTPIPES/conf/nextflow.containers.config` for more config options). 
Docker images can be used directly with singularity (e.g. ubuntu:latest). 
Nextflow will have Singularity convert the docker image to a .sif or .img file and store in the singularity `cacheDir` for re-use. 

## Manually building of images

To manually build the images for custom tools in this repo, you can do the following:

```bash
# cd into the images/ dir
cd images
# build the docker images
for image in *; do echo $image && cd $image && docker build -t nextpipes/$image . && cd ..; done
# create singularity image files from the docker images
# NOTE: that to build a singularity image from a LOCAL docker image, we use docker-daemon:// instead of docker://
for image in *; do singularity build $NEXTPIPES/singularity/$image.img docker-daemon://nextpipes/$image:latest
```

## Manually specifying a singularity image in Nextflow

To use a local .img or .sif file created by Singularity within Nextflow, specify the image path (/path/to/image.img).
To use a remote Docker image, just use the same syntax you would use for Docker (e.g. repo/image:tag).

### HINT HINT: 

For running a Singularity image manually, you can do a similar action as with Docker.

```bash
singularity run myimage.img /bin/bash
```