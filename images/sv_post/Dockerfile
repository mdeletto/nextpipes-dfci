FROM debian:bullseye

ARG TOOL_VERSION="2.0"
ARG REVISION="none"

LABEL org.opencontainers.image.title="sv-post"
LABEL org.opencontainers.image.version="$TOOL_VERSION"
LABEL org.opencontainers.image.authors="yupul@ds.dfci.harvard.edu , philk@ds.dfci.harvard.edu, stephens@ds.dfci.harvard.edu"
LABEL org.opencontainers.image.source="https://gitlab-ds.dfci.harvard.edu/vega/tools"
LABEL org.opencontainers.image.revision="$REVISION"
LABEL com.dfci.group="vega"
LABEL com.dfci.project="tools"

RUN export DEBIAN_FRONTEND="noninteractive" && \
apt-get update && \
apt-get install -y \
    build-essential \
    curl \
    libcurl4-gnutls-dev \
    libfontconfig1-dev \
    libgsl0-dev \
    libssl-dev \
    libxml2-dev \
    python3 \
    r-base \
    r-base-dev

#apt-get install -y \
#    build-essential \
#    curl \
#    libcurl4-gnutls-dev \
#    libfontconfig1-dev \
#    libgsl0-dev \
#    libssl-dev \
#    libxml2-dev \
#    python3 \
#    r-base=4.0.4-1 \
#    r-base-dev=4.0.4-1

RUN R -e 'install.packages(c( \
        "argparse", \
        "BiocManager", \
        "conflicted", \
        "covr", \
        "devtools", \
        "dplyr", \
        "DT", \
        "funr", \
        "here", \
        "readr", \
        "stringr", \
        "testthat" \
        ), dependencies = TRUE)' && \
    R -e 'BiocManager::install("GenomeInfoDb")' && \
    R -e 'BiocManager::install("Rsamtools")'

RUN apt-get install -y \
    libharfbuzz-dev \
    libfribidi-dev \
    libfreetype6-dev \
    libpng-dev \
    libtiff5-dev \
    libjpeg-dev

RUN R -e 'install.packages(c( \
        "textshaping", \
        "ragg", \
        "pkgdown", \
        "devtools" \
        ), dependencies = TRUE)'

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN echo "setwd(\"/app/\")" | tee /root/.Rprofile
WORKDIR /app/

RUN R -e 'BiocManager::install("GenomeInfoDb")' && \
    R -e 'BiocManager::install("Rsamtools")'

COPY ./src/ensemble_calling.R /app/ensemble_calling.R
COPY ./src/R/* /app/R/
COPY ./src/DESCRIPTION /app/DESCRIPTION
COPY ./src/.here /app/.here

# Add this for nextflow support. This will install 'ps' command
RUN apt-get update && apt-get install -y procps && rm -rf /var/lib/apt/lists/*

RUN chmod +x /app/ensemble_calling.R
