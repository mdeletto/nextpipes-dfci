#!/usr/bin/env Rscript

suppressMessages(library(dplyr))
suppressMessages(library(stringr))
suppressMessages(library(readr))
suppressMessages(library(Rsamtools))
suppressMessages(library(argparse))
options(warn = -1)
# avoid automatic parsing of string values into Factor items (categories/enums):
options(stringsAsFactors = FALSE)

######
# INPUT
######

parser <- ArgumentParser()
parser$add_argument("--output", help = "Output file. Fully-qualified path", action = 'store', dest = 'output')
parser$add_argument("--svaba-sv", help = "SV calls made by svaba. VCF file", action = 'store', dest = 'svaba_sv_file')
parser$add_argument("--svaba-indel", help = "Indels called by svaba. VCF file", action = 'store', dest = 'svaba_indel_file')
parser$add_argument("--svaba-bam", help = "BAM from svaba", action = 'store', dest = 'svaba_bam_file')
parser$add_argument("--manta-sv-indel", help = "SV/indel calls made by manta. VCF file", action = 'store', dest = 'manta_sv_indel_file')
parser$add_argument("--target-bed", help = "Assay target file", action = 'store', dest = 'target_bed_file')
parser$add_argument("--pon-range", help = "PON for 100bp range coordindates", action = 'store', dest = 'pon_range_file')
parser$add_argument("--repeat-mask", help = "Repeat mask bed file", action = 'store', dest = 'repeat_mask_file')
parser$add_argument("--breakmer-filter-list", help = "Breakmer filter list", action = 'store', dest = 'breakmer_filter_list_file')
parser$add_argument("--blacklist", help = "Blacklist", action = 'store', dest = 'blacklist_file')
parser$add_argument("--mode", help = "somatic or germline. Default somatic", default = "somatic", action = 'store', dest = 'mode')

# Parse args
args <- parser$parse_args()

######
# Verify inputs
######
output_file <- args$output

is_germline_mode <- FALSE
if ('germline' == args$mode) {
  is_germline_mode <- TRUE
}

if (file.access(args$svaba_sv_file) == -1) {
  stop(sprintf("ERROR: Svaba sv file ( %s ) does not exist!", args$svaba_sv_file))
}else {
  svaba_sv <- read.delim(text = grep("##", readLines(args$svaba_sv_file), value = TRUE, invert = T), header = T)
}

if (file.access(args$svaba_indel_file) == -1) {
  stop(sprintf("ERROR: Svaba indel file ( %s ) does not exist!", args$svaba_indel_file))
}else {
  svaba_indel <- read.delim(text = grep("##", readLines(args$svaba_indel_file), value = TRUE, invert = T), header = T)
}

if (file.access(args$svaba_bam_file) == -1) {
  stop(sprintf("ERROR: Svaba BAM file ( %s ) does not exist!", args$svaba_bam_file))
}else {
  # read in .bam output from svaba
  bamFile <- scanBam(args$svaba_bam_file)
}

if (file.access(args$target_bed_file) == -1) {
  stop(sprintf("ERROR: Target bed file ( %s ) does not exist!", args$target_bed_file))
}else {
  # read in targets bed file
  target_file <- suppressMessages(read_delim(args$target_bed_file, '\t', col_names = F, guess_max = 7000))
}

if (file.access(args$manta_sv_indel_file) == -1) {
  stop(sprintf("ERROR: Manta somatic sv file ( %s ) does not exist!", args$manta_sv_indel_file))
}else {
  manta <- read.delim(text = grep("##", readLines(args$manta_sv_indel_file), value = TRUE, invert = T), header = T)
}

if (file.access(args$pon_range_file) == -1) {
  stop(sprintf("ERROR: PON range file ( %s ) does not exist!", args$pon_range_file))
}else {
  PON_range <- suppressMessages(read_delim(args$pon_range_file, '\t'))
}

if (file.access(args$repeat_mask_file) == -1) {
  stop(sprintf("ERROR: Repeat mask file ( %s ) does not exist!", args$repeat_mask_file))
}else {
  # read in repeat mask file
  repeat.mask <- suppressMessages(read_delim(args$repeat_mask_file, '\t'))
}

if (file.access(args$breakmer_filter_list_file) == -1) {
  stop(sprintf("ERROR: breakmer filter list file ( %s ) does not exist!", args$breakmer_filter_list_file))
}else {
  # read in breakmer filter list file
  breakmer_filter_list <- suppressMessages(read_delim(args$breakmer_filter_list_file, '\t'))
}

if (file.access(args$blacklist_file) == -1) {
  stop(sprintf("ERROR: blacklist file ( %s ) does not exist!", args$blacklist_file))
}else {
  # read in blacklist file
  blacklist <- suppressMessages(read_delim(args$blacklist_file, '\t'))
}

source('/app/R/svaba_input.R')
svaba <- prepare_svaba(svaba_sv, svaba_indel, bamFile, target_file, is_germline_mode)

source('/app/R/manta_input.R')
manta <- prepare_manta(manta, target_file)

source('/app/R/overlap_manta_svabva.R')
overlaps <- overlap_manta_svaba(svaba, manta)

source('/app/R/annotate_filter_list.R')
PON_overlaps <- annotate_filter_list(overlaps, PON_range, "panel_of_normals")

source('/app/R/annotate_filter_list.R')
breakmer_filter_list_overlaps <- annotate_filter_list(PON_overlaps, breakmer_filter_list, "breakmer_filter_list")

source('/app/R/annotate_blacklist.R')
blacklist_overlaps <- annotate_blacklist(breakmer_filter_list_overlaps, blacklist, "blacklist")

source('/app/R/annotate_repeats.R')
repeats <- annotate_repeats(blacklist_overlaps, repeat.mask)

source('/app/R/output.R')
genes_and_output <- output(repeats)

# write final file
write_delim(genes_and_output, output_file, '\t')
