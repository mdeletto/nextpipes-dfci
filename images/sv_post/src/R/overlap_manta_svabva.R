
## This function takes the following inputs:

# svaba dataframe from svaba_input.R
# manta dataframe from manta_input.R

## The purpose of this function is to concatenate the svaba/manta output and
## collapse the overlapping SV/indel calls from svaba/manta

## The output of this function is a single dataframe called combined.svaba.manta.range.match

overlap_manta_svaba <- function(svaba, manta) {

    # intialize variables for position matching loop
    index_range <- 0
    index_range.count <- 1

    # save dataframes for potential exact/ranged position matches
    manta_range <- manta
    svaba_range <- svaba
    
    base_pair_span <- 100

    # loop over each svaba call to identify exact or range based matches
    # if match occurs label as such and retatin manta positions
    for (i in 1:nrow(svaba)) {

      for (j in 1:nrow(manta)) {

        if (isTRUE(svaba$CHROM1[i] == manta$CHROM1[j] & (svaba$POS1[i] <= (manta$POS1[j] + base_pair_span) & svaba$POS1[i] >= (manta$POS1[j] - base_pair_span))) &
            isTRUE(svaba$CHROM2[i] == manta$CHROM2[j] & (svaba$POS2[i] <= (manta$POS2[j] + base_pair_span) & svaba$POS2[i] >= (manta$POS2[j] - base_pair_span)))) {

          index_range[index_range.count] <- i
          index_range.count <- index_range.count + 1
          manta_range$CALLER[j] <- "manta_svaba"

        }

        if (isTRUE(svaba$CHROM1[i] == manta$CHROM2[j] & (svaba$POS1[i] <= (manta$POS2[j] + base_pair_span) & svaba$POS1[i] >= (manta$POS2[j] - base_pair_span))) &
            isTRUE(svaba$CHROM2[i] == manta$CHROM1[j] & (svaba$POS2[i] <= (manta$POS1[j] + base_pair_span) & svaba$POS2[i] >= (manta$POS1[j] - base_pair_span)))) {

          index_range[index_range.count] <- i
          index_range.count <- index_range.count + 1
          manta_range$CALLER[j] <- "manta_svaba"

        }

      }

    }

    # if ranged based matches occurred remove calls from svaba dataframe
    if (length(index_range) != 0) {
      if (index_range[1] > 0) {
        svaba_range <- svaba[-index_range,]
      }
    }

    combined.svaba.manta.range.match <- bind_rows(svaba_range, manta_range)

    # add this for next step
    if (nrow(combined.svaba.manta.range.match != 0)) {
     
       combined.svaba.manta.range.match$FILTER <- "PASS"
      
    }
    
    return (combined.svaba.manta.range.match)
}

