
## This function takes the follow inputs:

# unfiltered svaba sv vcf
# unfiltered svaba indel vcf
# contigs .bam svaba output
# target bed file (in reference dir)
# germline mode T or F

## The purpose of this function is to parse and concatenate the relevant svaba output

## The output of this function is a single dataframe called svaba

library(Rsamtools)
library(dplyr)
library(stringr)

prepare_svaba <- function(svaba_sv, svaba_indel, bamFile, target_file, is_germline_mode = FALSE) {

  if (nrow(svaba_sv) != 0) {

    svaba_sv$TYPE = "rearrangement"
    svaba_sv$ID <- as.character(svaba_sv$ID)
    svaba_sv$X.CHROM <- as.character(svaba_sv$X.CHROM)
    svaba_sv$REF <- as.character(svaba_sv$REF)
    svaba_sv$ALT <- as.character(svaba_sv$ALT)
  }

  if (nrow(svaba_indel) != 0) {

    svaba_indel$TYPE = "indel"
    svaba_indel$ID <- as.character(svaba_indel$ID)
    svaba_indel$X.CHROM <- as.character(svaba_indel$X.CHROM)
    svaba_indel$REF <- as.character(svaba_indel$REF)
    svaba_indel$ALT <- as.character(svaba_indel$ALT)
  }


  if (nrow(svaba_sv) != 0 & nrow(svaba_indel) != 0) {

    svaba <- bind_rows(svaba_sv, svaba_indel)

  } else if (nrow(svaba_sv) != 0) {

    svaba <- svaba_sv

  } else {

    svaba <- svaba_indel

  }

  # check if svaba input is empty
  if (nrow(svaba) != 0) {

    # if normal sample present delete column 10
    if (ncol(svaba) == 12) {

      svaba <- svaba[, -10]

    }

    # rename columns
    names(svaba)[1:2] <- c("CHROM1", "POS1")
    names(svaba)[10] <- "READS"

    # set required datatypes
    svaba$CONTIG_NAME <- NA
    svaba$REF <- as.character(svaba$REF)
    svaba$ALT <- as.character(svaba$ALT)
    svaba$CHROM1 <- as.character(svaba$CHROM1)

    # remove duplicate calls based on svaba IDs
    svaba$ID <- gsub(":.*", "", svaba$ID)

    if (length(which(duplicated(svaba$ID))) != 0) {
      svaba <- svaba[-which(duplicated(svaba$ID)),]
    }

    # remove calls without PASS filter
    svaba <- svaba %>% dplyr::filter(FILTER == "PASS" | FILTER == "NONVAR")

    # check if germline sample and filter appropriate indel size
    if (is_germline_mode) {

      svaba <- svaba[-which(!str_detect(svaba$ALT, "\\[|\\]") & (nchar(svaba$REF) < 6 & nchar(svaba$ALT) < 6)),]

    } else {

      svaba <- svaba[-which(!str_detect(svaba$ALT, "\\[|\\]") & (nchar(svaba$REF) < 16 & nchar(svaba$ALT) < 16)),]
    }

    # extract chromosome from SVs/indels
    svaba$CHROM2 <- as.character(str_split_fixed(gsub("\\[|\\]|[A|T|G|C]", "", svaba$ALT), ":", 2)[, 1])

    svaba$CHROM2 <- as.character(ifelse(svaba$CHROM2 == "", svaba$CHROM1, svaba$CHROM2))

    # extract or calculate position 2
    svaba$POS2 <- str_split_fixed(gsub("\\[|\\]|[A|T|G|C]", "", svaba$ALT), ":", 2)[, 2]
    svaba$POS2 <- as.numeric(ifelse(svaba$POS2 == "" & nchar(svaba$REF) > 1, svaba$POS1 + nchar(svaba$REF) - 1,
                                    ifelse(svaba$POS2 == "" & nchar(svaba$ALT) > 1, svaba$POS1, svaba$POS2)))

    # calculate SV length
    svaba$SVLEN <- as.character(
      ifelse(str_detect(svaba$ALT, "\\]|\\[") & svaba$CHROM1 != svaba$CHROM2, NA,
             ifelse(!str_detect(svaba$ALT, '\\[|\\]') & nchar(svaba$REF) > 1, nchar(svaba$REF) - 1,
                    ifelse(!str_detect(svaba$ALT, '\\[|\\]') & nchar(svaba$ALT) > 1, nchar(svaba$ALT) - 1, abs(svaba$POS2 - svaba$POS1))))
    )

    # extract discordant/split read counts
    svaba$DR <- str_split_fixed(svaba$READS, ":", 9)[, 7]
    svaba$SR <- str_split_fixed(svaba$READS, ":", 9)[, 6]
    svaba$DP <- as.character(str_split_fixed(svaba$READS, ":", 9)[, 3])

    # extract contig ID
    svaba$CONTIG_NAME <- gsub("SCTG=", "", str_extract(svaba$INFO, "SCTG=[^;]+"))

    # extract contig sequence and cigar string
    contig.id.bam <- bamFile[[1]]$qname
    contig.bam <- as.character(bamFile[[1]]$seq)
    cigar.bam <- bamFile[[1]]$cigar
    bam.df <- data.frame(CIGAR = cigar.bam, CONTIG_SEQ = contig.bam, CONTIG_NAME = contig.id.bam, CONTIG_CHR = bamFile[[1]]$rname, CONTIG_POS = bamFile[[1]]$pos)
    bam.df <- bam.df %>% distinct()

    # add info from bam to svaba dataframe
    svaba <- left_join(svaba, bam.df, by = "CONTIG_NAME")
    
    # retain only one contig per call
    svaba <- svaba[which(svaba$CHROM1 == svaba$CONTIG_CHR & svaba$CONTIG_POS < svaba$POS1), ]
    svaba <- svaba %>% select(-CONTIG_CHR, -CONTIG_POS)
    
    if (nrow(svaba) != 0) {

      # remove calls in which both positions do not overlap target bed file
      index <- 1
      index_to_filter <- 0

      for (i in 1:nrow(svaba)) {

        if ((sum(svaba$CHROM1[i] == target_file$X1 &
                   svaba$POS1[i] >= target_file$X2 &
                   svaba$POS1[i] <= target_file$X3) == 0 &
          sum(svaba$CHROM2[i] == target_file$X1 &
                svaba$POS2[i] >= target_file$X2 &
                svaba$POS2[i] <= target_file$X3) == 0))
        {

          index_to_filter[index] <- i
          index <- index + 1

        } else {

          next

        }

      }

      if (index_to_filter[1] != 0) {
        svaba <- svaba[-index_to_filter,]
      }

      if (nrow(svaba) == 0) {

        svaba <- data.frame()

      }

      # remove unnecessary column and add caller name
      if (nrow(svaba) != 0) {
        svaba <- svaba %>% select(-ID, -REF, -ALT, -QUAL, -FILTER, -INFO, -FORMAT, -READS)
        svaba$CALLER <- "svaba"
        svaba$CHROM1 <- as.character(svaba$CHROM1)
      }

    } else {

      svaba <- data.frame()

    } #ENDIF nrows != 0

  } else {

    # if input is empty assign empty dataframe
    svaba <- data.frame()

  }

  return(svaba)
}
